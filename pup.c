#include <stdio.h>
#include "pup.h"

#ifndef MPI_ONDES3D
void pup_imatrix(pup_er p, int ***im, int r_start, int r_end,
		 int c_start, int c_end)
{
  if(pup_isUnpacking(p)){
    *im = imatrix(r_start, r_end, c_start, c_end);
  }
  int *real_start = (*im)[r_start] + c_start;
  int n_rows = r_end - r_start + 1;
  int n_colums = c_end - c_start + 1;
  pup_ints(p, real_start, n_rows * n_colums);
  if(pup_isPacking(p)){
    free_imatrix(*im, r_start, r_end, c_start, c_end);
  }
}

void pup_dmatrix(pup_er p, double ***dm, int r_start, int r_end,
		 int c_start, int c_end)
{
  if(pup_isUnpacking(p)){
    *dm = dmatrix(r_start, r_end, c_start, c_end);
  }
  double *real_start = (*dm)[r_start] + c_start;
  int n_rows = r_end - r_start + 1;
  int n_colums = c_end - c_start + 1;
  pup_doubles(p, real_start, n_rows * n_colums);
  if(pup_isPacking(p)){
    free_dmatrix(*dm, r_start, r_end, c_start, c_end);
  }
}

void pup_i3tensor(pup_er p, int ****i3t, int x_start, int x_end,
		  int y_start, int y_end, int z_start, int z_end)
{
  if(pup_isUnpacking(p)){
    *i3t = i3tensor(x_start, x_end, y_start, y_end, z_start, z_end);
  }
  int *real_start = (*i3t)[x_start][y_start] + z_start;
  int dim_x = x_end - x_start + 1;
  int dim_y = y_end - y_start + 1;
  int dim_z = z_end - z_start + 1;
  pup_ints(p, real_start, dim_x * dim_y * dim_z);
  if(pup_isPacking(p)){
    free_i3tensor(*i3t, x_start, x_end, y_start, y_end, z_start, z_end);
  }
}

void pup_d3tensor(pup_er p, double ****d3t, int x_start, int x_end,
		  int y_start, int y_end, int z_start, int z_end)
{
  if(pup_isUnpacking(p)){
    *d3t = d3tensor(x_start, x_end, y_start, y_end, z_start, z_end);
  }
  double *real_start = (*d3t)[x_start][y_start] + z_start;
  int dim_x = x_end - x_start + 1;
  int dim_y = y_end - y_start + 1;
  int dim_z = z_end - z_start + 1;
  pup_doubles(p, real_start, dim_x * dim_y * dim_z);
  if(pup_isPacking(p)){
    free_d3tensor(*d3t, x_start, x_end, y_start, y_end, z_start, z_end);
  }
}

void pup_ivector(pup_er p, int **vector, int first, int last)
{
  if(pup_isUnpacking(p)){
    *vector = ivector(first, last);
  }
  int *real_start = *vector + first;
  int size = last - first + 1;
  pup_ints(p, real_start, size);
  if(pup_isPacking(p)){
    free_ivector(*vector, first, last);
  }
}

void pup_dvector(pup_er p, double **vector, int first, int last)
{
  if(pup_isUnpacking(p)){
    *vector = dvector(first, last);
  }
  double *real_start = *vector + first;
  int size = last - first + 1;
  pup_doubles(p, real_start, size);
  if(pup_isPacking(p)){
    free_dvector(*vector, first, last);
  }
}

void pup_parameters(pup_er p, void *prm_struct)
{
  struct PARAMETERS *prm = (struct PARAMETERS *) prm_struct;
  int i2imp_begin, i2imp_end, j2jmp_begin, j2jmp_end,
    i2icpu_begin, i2icpu_end, j2jcpu_begin, j2jcpu_end;
  pup_double(p, &(prm->dt));
  pup_double(p, &(prm->ds));
  pup_int(p, &(prm->xMin));
  pup_int(p, &(prm->xMax));
  pup_int(p, &(prm->yMin));
  pup_int(p, &(prm->yMax));
  pup_int(p, &(prm->zMin));
  pup_int(p, &(prm->zMax));
  pup_int(p, &(prm->delta));
  pup_int(p, &(prm->zMax0));
  pup_double(p, &(prm->x0));
  pup_double(p, &(prm->y0));
  pup_double(p, &(prm->z0));
  pup_int(p, &(prm->me));
  pup_ints(p, prm->coords, 2);
  pup_int(p, &(prm->px));
  pup_int(p, &(prm->py));
  pup_int(p, &(prm->mpmx));
  pup_int(p, &(prm->mpmy));
  pup_long(p, &(prm->nmaxx));
  pup_long(p, &(prm->nmaxy));


  /*Debugging     
     printf("mpmx: %d; mpmy: %d; delta: %d;\n", prm->mpmx, prm->mpmy,
     prm->delta);
     printf("xMin: %d; xMax: %d;\nyMin: %d; yMax: %d;\n", prm->xMin,
     prm->xMax, prm->yMin, prm->yMax);
     fflush(stdout);
   */

  pup_ivector(p, &(prm->imp2i_array), -1, prm->mpmx + 4);
  pup_ivector(p, &(prm->jmp2j_array), -1, prm->mpmy + 4);
  pup_ivector(p, &(prm->i2imp_array), prm->xMin - prm->delta,
	      prm->xMax + 2 * prm->delta + 2);
  pup_ivector(p, &(prm->j2jmp_array), prm->yMin - prm->delta,
	      prm->yMax + 2 * prm->delta + 2);
  pup_ivector(p, &(prm->i2icpu_array), prm->xMin - prm->delta,
	      prm->xMax + 2 * prm->delta + 2);
  pup_ivector(p, &(prm->j2jcpu_array), prm->yMin - prm->delta,
	      prm->yMax + 2 * prm->delta + 2);
  pup_ivector(p, &(prm->mpmx_tab), 0, prm->px - 1);
  pup_ivector(p, &(prm->mpmy_tab), 0, prm->py - 1);

  pup_double(p, &(prm->pi));
  pup_chars(p, prm->dir, FNAME_LENGTH);
  pup_chars(p, prm->fsrcMap, FNAME_LENGTH);
  pup_chars(p, prm->fsrcHist, FNAME_LENGTH);
  pup_chars(p, prm->fstatMap, FNAME_LENGTH);
  pup_chars(p, prm->fgeo, FNAME_LENGTH);
}

int pup_medium(pup_er p, struct MEDIUM *MDM, struct PARAMETERS *PRM)
{
  int i;
  pup_int(p, &(MDM->nLayer));
  pup_int(p, &(MDM->nLayer2));
  if(model == GEOLOGICAL){
    if(pup_isUnpacking(p)){
      MDM->name_mat = calloc(MDM->nLayer, sizeof(char *));
      for(i = 0; i < MDM->nLayer; i++){
	MDM->name_mat[i] = calloc(STRL, sizeof(char));
      }
    }
    pup_i3tensor(p, &MDM->imed, -1, PRM->mpmx + 2, -1,
		 PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
    pup_int(p, &(MDM->numVoid));
    pup_int(p, &(MDM->numSea));
    for(i = 0; i < MDM->nLayer; i++){
      pup_chars(p, MDM->name_mat[i], STRL);
    }

    if(pup_isPacking(p)){
      for(i = 0; i < MDM->nLayer; i++){
	free(MDM->name_mat[i]);
      }
      free(MDM->name_mat);
    }
  }else if(model == LAYER){
    if(pup_isUnpacking(p)){
    }
    pup_dvector(p, &(MDM->laydep), 0, MDM->nLayer - 1);

    /*FIXME: MDM->laydep2 seems to be of variable size. Se the code
     * in the file "alloAndInit_LayerModel.c"*/
    pup_dvector(p, &(MDM->laydep2), 0, MDM->nLayer2 - 1);

    pup_ivector(p, &(MDM->k2ly0), PRM->zMin - PRM->delta, PRM->zMax0);
    pup_ivector(p, &(MDM->k2ly2), PRM->zMin - PRM->delta, PRM->zMax0);

    if(pup_isPacking(p)){
    }
  }
  pup_dvector(p, &(MDM->rho0), 0, MDM->nLayer - 1);
  pup_dvector(p, &(MDM->mu0), 0, MDM->nLayer - 1);
  pup_dvector(p, &(MDM->kap0), 0, MDM->nLayer - 1);

  //FIXME: I'm not sure about the size of the next three vectors.
  pup_dvector(p, &(MDM->rho2), 0, MDM->nLayer2 - 1);
  pup_dvector(p, &(MDM->mu2), 0, MDM->nLayer2 - 1);
  pup_dvector(p, &(MDM->kap2), 0, MDM->nLayer2 - 1);

}


int pup_source(pup_er p, struct SOURCE *SRC, struct PARAMETERS *PRM)
{
  /*Replace by calls to PUP functions.
     if(pup_isUnpacking(p)){
     SRC->insrc  = ivector(0, SRC->iSrc - 1);
     SRC->ixhypo = ivector(0, SRC->iSrc - 1);
     SRC->iyhypo = ivector(0, SRC->iSrc - 1);
     SRC->izhypo = ivector(0, SRC->iSrc - 1);
     if (sourceType == HISTFILE){ 
     SRC->strike = mydvector0(0, SRC->iSrc - 1);
     SRC->dip = mydvector0(0, SRC->iSrc - 1);
     SRC->rake = mydvector0(0, SRC->iSrc - 1);
     SRC->slip = mydvector0(0, SRC->iSrc - 1);
     SRC->xweight = mydvector0(0, SRC->iSrc - 1);
     SRC->yweight = mydvector0(0, SRC->iSrc - 1);
     SRC->zweight = mydvector0(0, SRC->iSrc - 1);
     SRC->vel = dmatrix(0, SRC->iSrc - 1, 0, SRC->iDur - 1);
     SRC->fx = myd3tensor0 (1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     SRC->fy = myd3tensor0 (1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     SRC->fz = myd3tensor0 (1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     }
     }
     pup_ints(p, SRC->insrc, SRC->iSrc);
     pup_ints(p, SRC->ixhypo, SRC->iSrc);
     pup_ints(p, SRC->iyhypo, SRC->iSrc);
     pup_ints(p, SRC->izhypo, SRC->iSrc);
     if (sourceType == HISTFILE){ 
     pup_doubles(p, SRC->strike, SRC->iSrc);
     pup_doubles(p, SRC->dip, SRC->iSrc);
     pup_doubles(p, SRC->rake, SRC->iSrc);
     pup_doubles(p, SRC->slip, SRC->iSrc);
     pup_doubles(p, SRC->xweight, SRC->iSrc);
     pup_doubles(p, SRC->yweight, SRC->iSrc);
     pup_doubles(p, SRC->zweight, SRC->iSrc);
     pup_dmatrix(p, &SRC->vel, 0, SRC->iSrc - 1, 0, SRC->iDur - 1);
     pup_d3tensor(p, &SRC->fx, 1, PRM->mpmx, 1, PRM->mpmy, 
     PRM->zMin-PRM->delta, PRM->zMax0);
     pup_d3tensor(p, &SRC->fy, 1, PRM->mpmx, 1, PRM->mpmy, 
     PRM->zMin-PRM->delta, PRM->zMax0);
     pup_d3tensor(p, &SRC->fz, 1, PRM->mpmx, 1, PRM->mpmy, 
     PRM->zMin-PRM->delta, PRM->zMax0);
     }
     if(pup_isPacking(p)){
     free_ivector (SRC->insrc, 0, SRC->iSrc - 1);
     free_ivector (SRC->ixhypo,0, SRC->iSrc - 1);
     free_ivector (SRC->iyhypo, 0, SRC->iSrc - 1);
     free_ivector (SRC->izhypo, 0, SRC->iSrc - 1);
     if (sourceType == HISTFILE){
     free_dvector(SRC->strike, 0, SRC->iSrc - 1);
     free_dvector(SRC->dip, 0, SRC->iSrc - 1);
     free_dvector(SRC->rake, 0, SRC->iSrc - 1);
     free_dvector(SRC->slip, 0, SRC->iSrc - 1);
     free_dvector(SRC->xweight, 0, SRC->iSrc - 1);
     free_dvector(SRC->yweight, 0, SRC->iSrc - 1);
     free_dvector(SRC->zweight, 0, SRC->iSrc - 1);
     free_dmatrix (SRC->vel, 0, SRC->iSrc - 1, 0,
     SRC->iDur - 1);
     free_d3tensor (SRC->fx, 1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     free_d3tensor (SRC->fy, 1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     free_d3tensor (SRC->fz, 1, PRM->mpmx, 1, PRM->mpmy,
     PRM->zMin-PRM->delta, PRM->zMax0);
     }
     } */

  pup_int(p, &(SRC->iSrc));
  pup_ivector(p, &SRC->insrc, 0, SRC->iSrc - 1);
  pup_int(p, &(SRC->ixhypo0));
  pup_int(p, &(SRC->iyhypo0));
  pup_int(p, &(SRC->izhypo0));
  pup_double(p, &(SRC->xhypo0));
  pup_double(p, &(SRC->yhypo0));
  pup_double(p, &(SRC->zhypo0));
  pup_ivector(p, &SRC->ixhypo, 0, SRC->iSrc - 1);
  pup_ivector(p, &SRC->iyhypo, 0, SRC->iSrc - 1);
  pup_ivector(p, &SRC->izhypo, 0, SRC->iSrc - 1);
  pup_double(p, &(SRC->mo));
  pup_double(p, &(SRC->mw));
  pup_double(p, &(SRC->dsbiem));
  pup_double(p, &(SRC->dtbiem));
  pup_int(p, &(SRC->iDur));
  if(sourceType == HISTFILE){
    pup_dvector(p, &SRC->strike, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->dip, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->rake, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->slip, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->xweight, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->yweight, 0, SRC->iSrc - 1);
    pup_dvector(p, &SRC->zweight, 0, SRC->iSrc - 1);
    pup_dmatrix(p, &SRC->vel, 0, SRC->iSrc - 1, 0, SRC->iDur - 1);
    pup_d3tensor(p, &SRC->fx, 1, PRM->mpmx, 1, PRM->mpmy,
		 PRM->zMin - PRM->delta, PRM->zMax0);
    pup_d3tensor(p, &SRC->fy, 1, PRM->mpmx, 1, PRM->mpmy,
		 PRM->zMin - PRM->delta, PRM->zMax0);
    pup_d3tensor(p, &SRC->fz, 1, PRM->mpmx, 1, PRM->mpmy,
		 PRM->zMin - PRM->delta, PRM->zMax0);
  }
}

int pup_anelasticity(pup_er p, struct ANELASTICITY *ANL, struct MEDIUM *MDM,
		     struct PARAMETERS *PRM)
{
  if(ANLmethod == KRISTEKandMOCZO || ANLmethod == DAYandBRADLEY){
    pup_dvector(p, &ANL->Qp0, 0, MDM->nLayer - 1);
    pup_dvector(p, &ANL->Qs0, 0, MDM->nLayer - 1);

    /*FIXME Chek if using the right indexes. */
    pup_dvector(p, &ANL->Qp2, 0, MDM->nLayer2 - 1);
    pup_dvector(p, &ANL->Qs2, 0, MDM->nLayer2 - 1);
    if(ANLmethod == DAYandBRADLEY){
      pup_d3tensor(p, &(ANL->ksixx), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksiyy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksizz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksixy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksixz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksiyz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fxx), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fyy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fzz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fxy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fxz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->fyz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_double(p, &(ANL->w0));
      pup_double(p, &(ANL->tm));
      pup_double(p, &(ANL->tM));
    }else{			//ANLmethod == KRISTEKandMOCZO
      pup_d3tensor(p, &(ANL->ksilxx), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksilyy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksilzz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksilxy), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksilxz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);
      pup_d3tensor(p, &(ANL->ksilyz), -1, PRM->mpmx + 2, -1,
		   PRM->mpmy + 2, PRM->zMin - PRM->delta, PRM->zMax0);

      pup_dmatrix(p, &(ANL->ylmu), 1, 4, 0, MDM->nLayer - 1);
      pup_dmatrix(p, &(ANL->ylkap), 1, 4, 0, MDM->nLayer - 1);
      /*FIXME: check these indexes. */
      pup_dmatrix(p, &(ANL->ylmu2), 1, 4, 0, MDM->nLayer2 - 1);
      pup_dmatrix(p, &(ANL->ylkap2), 1, 4, 0, MDM->nLayer2 - 1);
      pup_dvector(p, &(ANL->wl), 1, 4);
      pup_double(p, &(ANL->wmin));
      pup_double(p, &(ANL->wmax));

    }
  }else if(ANLmethod == ANOTHER){
    pup_dvector(p, &ANL->q0, 0, MDM->nLayer - 1);
    pup_dvector(p, &ANL->amp, 0, MDM->nLayer - 1);
    pup_dvector(p, &ANL->q2, 0, MDM->nLayer2 - 1);
    pup_dvector(p, &ANL->amp2, 0, MDM->nLayer2 - 1);
  }


}

void pup_absorbing_boundary_condition(pup_er p,
				      struct ABSORBING_BOUNDARY_CONDITION
				      *ABC, struct PARAMETERS *PRM)
{
  pup_double(p, &(ABC->dump0));
  pup_double(p, &(ABC->nPower));
  pup_double(p, &(ABC->reflect));
  pup_int(p, &(ABC->npmlv));
  pup_int(p, &(ABC->npmlt));
  pup_i3tensor(p, &(ABC->ipml), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_dvector(p, &(ABC->dumpx), 1, PRM->mpmx);
  pup_dvector(p, &(ABC->dumpy), 1, PRM->mpmy);
  pup_dvector(p, &(ABC->dumpz), PRM->zMin - PRM->delta, PRM->zMax0);
  pup_dvector(p, &(ABC->dumpx2), 1, PRM->mpmx);
  pup_dvector(p, &(ABC->dumpy2), 1, PRM->mpmy);
  pup_dvector(p, &(ABC->dumpz2), PRM->zMin - PRM->delta, PRM->zMax0);

  pup_double(p, &(ABC->fd));
  pup_double(p, &(ABC->alpha0));
  pup_double(p, &(ABC->kappa0));
  if(ABCmethod == CPML){
    pup_dvector(p, &(ABC->phivxx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivxy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivxz), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivyx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivyy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivyz), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivzx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivzy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->phivzz), 1, ABC->npmlv);

    pup_dvector(p, &(ABC->phitxxx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phitxyy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phitxzz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phitxyx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phityyy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phityzz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phitxzx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phityzy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->phitzzz), 1, ABC->npmlt);

    pup_dvector(p, &(ABC->kappax), 1, PRM->mpmx);
    pup_dvector(p, &(ABC->alphax), 1, PRM->mpmx);
    pup_dvector(p, &(ABC->kappax2), 1, PRM->mpmx);
    pup_dvector(p, &(ABC->alphax2), 1, PRM->mpmx);
    pup_dvector(p, &(ABC->kappay), 1, PRM->mpmy);
    pup_dvector(p, &(ABC->alphay), 1, PRM->mpmy);
    pup_dvector(p, &(ABC->kappay2), 1, PRM->mpmy);
    pup_dvector(p, &(ABC->alphay2), 1, PRM->mpmy);

    pup_dvector(p, &(ABC->kappaz), PRM->zMin - PRM->delta, PRM->zMax0);
    pup_dvector(p, &(ABC->alphaz), PRM->zMin - PRM->delta, PRM->zMax0);
    pup_dvector(p, &(ABC->kappaz2), PRM->zMin - PRM->delta, PRM->zMax0);
    pup_dvector(p, &(ABC->alphaz2), PRM->zMin - PRM->delta, PRM->zMax0);
  }else if(ABCmethod == PML){
    pup_dvector(p, &(ABC->vxx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vxy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vxz), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vyx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vyy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vyz), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vzx), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vzy), 1, ABC->npmlv);
    pup_dvector(p, &(ABC->vzz), 1, ABC->npmlv);

    pup_dvector(p, &(ABC->txxx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txxy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txxz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tyyx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tyyy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tyyz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tzzx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tzzy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tzzz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txyx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txyy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txzx), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->txzz), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tyzy), 1, ABC->npmlt);
    pup_dvector(p, &(ABC->tyzz), 1, ABC->npmlt);
  }
}

void pup_velocity(pup_er p, struct VELOCITY *v, struct PARAMETERS *PRM)
{
  pup_d3tensor(p, &(v->x), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(v->y), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(v->z), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
}

void pup_stress(pup_er p, struct STRESS *s, struct PARAMETERS *PRM)
{
  pup_d3tensor(p, &(s->xx), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(s->yy), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(s->zz), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(s->xy), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(s->xz), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
  pup_d3tensor(p, &(s->yz), -1, PRM->mpmx + 2, -1, PRM->mpmy + 2,
	       PRM->zMin - PRM->delta, PRM->zMax0);
}

void pup_outputs(pup_er p, struct OUTPUTS *OUT)
{
  pup_int(p, &(OUT->iObs));
  pup_ivector(p, &(OUT->ixobs), 0, OUT->iObs - 1);
  pup_ivector(p, &(OUT->iyobs), 0, OUT->iObs - 1);
  pup_ivector(p, &(OUT->izobs), 0, OUT->iObs - 1);

  pup_dvector(p, &(OUT->xobs), 0, OUT->iObs - 1);
  pup_dvector(p, &(OUT->yobs), 0, OUT->iObs - 1);
  pup_dvector(p, &(OUT->zobs), 0, OUT->iObs - 1);

  pup_ivector(p, &(OUT->nobs), 0, OUT->iObs - 1);
  pup_ivector(p, &(OUT->ista), 0, OUT->iObs - 1);

  pup_dvector(p, &(OUT->xobswt), 0, OUT->iObs - 1);
  pup_dvector(p, &(OUT->yobswt), 0, OUT->iObs - 1);
  pup_dvector(p, &(OUT->zobswt), 0, OUT->iObs - 1);

  pup_imatrix(p, &(OUT->mapping_seis), 0, OUT->iObs - 1, 1, 9);

  pup_d3tensor(p, &(OUT->seis_output), 0, STATION_STEP - 1, 0,
	       OUT->iObs - 1, 1, 9);
  pup_dvector(p, &(OUT->seis_buff), 0, STATION_STEP - 1);

  pup_int(p, &(OUT->i0));
  pup_int(p, &(OUT->j0));
  pup_int(p, &(OUT->k0));
  pup_long(p, &(OUT->test_size));
  pup_dvector(p, &(OUT->snapBuff), 1, OUT->test_size);

  /*This is not needed. The V*global variables are alocated and freed
   * in each iteration.*/
/*	
	int icpu = PRM->j2jcpu_array[OUT.j0];
	int jcpu =  PRM->i2icpu_array[OUT.i0];
	if(PRM->me == 0){
		pup_dmatrix(p, &(OUT->Vxglobal), PRM->xMin - PRM->delta, 
			PRM->xMax + PRM->delta + 2, PRM->yMin - PRM->delta, 
			PRM->yMax + PRM->delta + 2);
		pup_dmatrix(p, &(OUT->Vyglobal), PRM->xMin - PRM->delta, 
			PRM->xMax + PRM->delta + 2, PRM->yMin - PRM->delta, 
			PRM->yMax + PRM->delta + 2);
		pup_dmatrix(p, &(OUT->Vzglobal), PRM->xMin - PRM->delta, 
			PRM->xMax + PRM->delta + 2, PRM->yMin - PRM->delta, 
			PRM->yMax + PRM->delta + 2);
	}else if(PRM->me == (jcpu * PRM->px)){
		pup_dmatrix(p, &(OUT->Vxglobal), PRM->xMin - PRM->delta, 
			PRM->xMax + PRM->delta + 2, PRM->zMin - PRM->delta, 
			PRM->zMax0);
		pup_dmatrix(p, &(OUT->Vyglobal), PRM->xMin - PRM->delta, 
				PRM->xMax + PRM->delta + 2, 
				PRM->zMin - PRM->delta, PRM->zMax0);
		pup_dmatrix(p, &(OUT->Vzglobal), PRM->xMin - PRM->delta, 
				PRM->xMax + PRM->delta + 2, 
				PRM->zMin - PRM->delta, PRM->zMax0);
	}else if(PRM->me == icpu){
		pup_dmatrix(p, &(OUT->Vxglobal), PRM->yMin - PRM->delta, 
				PRM->yMax + PRM->delta + 2, 
				PRM->zMin - PRM->delta, PRM->zMax0);
		pup_dmatrix(p, &(OUT->Vyglobal), PRM->yMin - PRM->delta, 
				PRM->yMax + PRM->delta + 2, 
				PRM->zMin - PRM->delta, PRM->zMax0);
		pup_dmatrix(p, &(OUT->Vzglobal), PRM->yMin - PRM->delta, 
				PRM->yMax + PRM->delta + 2, 
				PRM->zMin - PRM->delta, PRM->zMax0);
	}
	if(snapType == ODISPL || snapType == OBOTH){
		pup_d3tensor(p, &(OUT->Uxy), 1, 3, -1, PRM->mpmx + 2, -1,
			PRM->mpmy + 2);
		pup_d3tensor(p, &(OUT->Uyz),1, 3, -1, PRM->mpmx + 2,
			PRM->zMin - PRM->delta, PRM->zMax0);
		pup_d3tensor(p, &(OUT->Uxz),1, 3, -1, PRM->mpmy + 2,
			PRM->zMin - PRM->delta, PRM->zMax0);
	}
*/
  pup_long(p, &(OUT->total_prec_x));
  pup_long(p, &(OUT->total_prec_y));
}

void pup_comm_directions(pup_er p, struct comm_directions *cd)
{
  struct COMM_DIRECTION *north = &(cd->NORTH);
  pup_long(p, &(north->nmax));
  pup_int(p, &(north->rank));

  pup_int(p, &(north->iMinS));
  pup_int(p, &(north->iMaxS));
  pup_int(p, &(north->jMinS));
  pup_int(p, &(north->jMaxS));
  pup_dvector(p, &(north->bufV0S), 0, 3 * north->nmax - 1);
  pup_int(p, &(north->channelV0S));
  pup_dvector(p, &(north->bufT0S), 0, 6 * north->nmax - 1);


  pup_int(p, &(north->iMinR));
  pup_int(p, &(north->iMaxR));
  pup_int(p, &(north->jMinR));
  pup_int(p, &(north->jMaxR));
  pup_dvector(p, &(north->bufV0R), 0, 3 * north->nmax - 1);
  pup_int(p, &(north->channelV0R));
  pup_dvector(p, &(north->bufT0R), 0, 6 * north->nmax - 1);
  pup_int(p, &(north->channelT0R));
  pup_int(p, &(north->channelT0S));

  if(ANLmethod == KRISTEKandMOCZO){
    pup_dvector(p, &(north->bufKsiS), 0, 6 * north->nmax - 1);
    pup_int(p, &(north->channelKsiS));

    pup_dvector(p, &(north->bufKsiR), 0, 6 * north->nmax - 1);
    pup_int(p, &(north->channelKsiR));
  }

  struct COMM_DIRECTION *east = &(cd->EAST);
  pup_long(p, &(east->nmax));
  pup_int(p, &(east->rank));

  pup_int(p, &(east->iMinS));
  pup_int(p, &(east->iMaxS));
  pup_int(p, &(east->jMinS));
  pup_int(p, &(east->jMaxS));
  pup_dvector(p, &(east->bufV0S), 0, 3 * east->nmax - 1);
  pup_int(p, &(east->channelV0S));
  pup_dvector(p, &(east->bufT0S), 0, 6 * east->nmax - 1);


  pup_int(p, &(east->iMinR));
  pup_int(p, &(east->iMaxR));
  pup_int(p, &(east->jMinR));
  pup_int(p, &(east->jMaxR));
  pup_dvector(p, &(east->bufV0R), 0, 3 * east->nmax - 1);
  pup_int(p, &(east->channelV0R));
  pup_dvector(p, &(east->bufT0R), 0, 6 * east->nmax - 1);
  pup_int(p, &(east->channelT0R));
  pup_int(p, &(east->channelT0S));

  if(ANLmethod == KRISTEKandMOCZO){
    pup_dvector(p, &(east->bufKsiS), 0, 6 * east->nmax - 1);
    pup_int(p, &(east->channelKsiS));

    pup_dvector(p, &(east->bufKsiR), 0, 6 * east->nmax - 1);
    pup_int(p, &(east->channelKsiR));
  }

  struct COMM_DIRECTION *south = &(cd->SOUTH);
  pup_long(p, &(south->nmax));
  pup_int(p, &(south->rank));

  pup_int(p, &(south->iMinS));
  pup_int(p, &(south->iMaxS));
  pup_int(p, &(south->jMinS));
  pup_int(p, &(south->jMaxS));
  pup_dvector(p, &(south->bufV0S), 0, 3 * south->nmax - 1);
  pup_int(p, &(south->channelV0S));
  pup_dvector(p, &(south->bufT0S), 0, 6 * south->nmax - 1);


  pup_int(p, &(south->iMinR));
  pup_int(p, &(south->iMaxR));
  pup_int(p, &(south->jMinR));
  pup_int(p, &(south->jMaxR));
  pup_dvector(p, &(south->bufV0R), 0, 3 * south->nmax - 1);
  pup_int(p, &(south->channelV0R));
  pup_dvector(p, &(south->bufT0R), 0, 6 * south->nmax - 1);
  pup_int(p, &(south->channelT0R));
  pup_int(p, &(south->channelT0S));

  if(ANLmethod == KRISTEKandMOCZO){
    pup_dvector(p, &(south->bufKsiS), 0, 6 * south->nmax - 1);
    pup_int(p, &(south->channelKsiS));

    pup_dvector(p, &(south->bufKsiR), 0, 6 * south->nmax - 1);
    pup_int(p, &(south->channelKsiR));
  }

  struct COMM_DIRECTION *west = &(cd->WEST);
  pup_long(p, &(west->nmax));
  pup_int(p, &(west->rank));

  pup_int(p, &(west->iMinS));
  pup_int(p, &(west->iMaxS));
  pup_int(p, &(west->jMinS));
  pup_int(p, &(west->jMaxS));
  pup_dvector(p, &(west->bufV0S), 0, 3 * west->nmax - 1);
  pup_int(p, &(west->channelV0S));
  pup_dvector(p, &(west->bufT0S), 0, 6 * west->nmax - 1);


  pup_int(p, &(west->iMinR));
  pup_int(p, &(west->iMaxR));
  pup_int(p, &(west->jMinR));
  pup_int(p, &(west->jMaxR));
  pup_dvector(p, &(west->bufV0R), 0, 3 * west->nmax - 1);
  pup_int(p, &(west->channelV0R));
  pup_dvector(p, &(west->bufT0R), 0, 6 * west->nmax - 1);
  pup_int(p, &(west->channelT0R));
  pup_int(p, &(west->channelT0S));

  if(ANLmethod == KRISTEKandMOCZO){
    pup_dvector(p, &(west->bufKsiS), 0, 6 * west->nmax - 1);
    pup_int(p, &(west->channelKsiS));

    pup_dvector(p, &(west->bufKsiR), 0, 6 * west->nmax - 1);
    pup_int(p, &(west->channelKsiR));
  }
}

void pup_timing(pup_er p, struct timing *t)
{
  pup_double(p, &(t->bc1));
  pup_double(p, &(t->bc2));
  pup_double(p, &(t->comm1));
  pup_double(p, &(t->comm2));
  pup_double(p, &(t->start));
  pup_double(p, &(t->end));
  pup_double(p, &(t->total));
}

void pup_chunk(pup_er p, void *chunk_prm)
{
  struct chunk_struct *chunk = (struct chunk_struct *) chunk_prm;

/*struct PARAMETERS***********************************************************/
  struct PARAMETERS *PRM = &(chunk->PRM);
  pup_parameters(p, PRM);
/*****************************************************************************/
//      printf("%d: Puped PARAMETERS.\n", PRM->me);

/*struct SOURCE***************************************************************/
  struct SOURCE *SRC = &(chunk->SRC);
  pup_source(p, SRC, PRM);
/*****************************************************************************/
//      printf("%d: Puped SOURCE.\n", PRM->me);

/*struct MEDIUM****************************************************************/
  struct MEDIUM *MDM = &(chunk->MDM);
  pup_medium(p, MDM, PRM);
/*****************************************************************************/
//      printf("%d: Puped MEDIUM.\n", PRM->me);

/*struct ANELASTICITY*********************************************************/
  struct ANELASTICITY *ANL = &(chunk->ANL);
  pup_anelasticity(p, ANL, MDM, PRM);
/*****************************************************************************/
//      printf("%d: Puped ANELASTICITY.\n", PRM->me);

/*struct ABSORBING_BOUNDARY_CONDITION ****************************************/
  struct ABSORBING_BOUNDARY_CONDITION *ABC = &(chunk->ABC);
  pup_absorbing_boundary_condition(p, ABC, PRM);
/*****************************************************************************/
//      printf("%d: Puped ABC.\n", PRM->me);

/* struct VELOCITY ***********************************************************/
  struct VELOCITY *v = &(chunk->v0);
  pup_velocity(p, v, PRM);
/*****************************************************************************/
//      printf("%d: Puped VELOCITY.\n", PRM->me);

/* struct STRESS *************************************************************/
  struct STRESS *s = &(chunk->t0);
  pup_stress(p, s, PRM);
/*****************************************************************************/
//      printf("%d: Puped STRESS.\n", PRM->me);

/* struct OUTPUT *************************************************************/
  struct OUTPUTS *OUT = &(chunk->OUT);
  pup_outputs(p, OUT);
/*****************************************************************************/
//      printf("%d: Puped OUTPUT.\n", PRM->me);

/* struct comm_directions ****************************************************/
  struct comm_directions *cd = &(chunk->c_directions);
  pup_comm_directions(p, cd);
/*****************************************************************************/
//      printf("%d: Puped comm_directions.\n", PRM->me);

/* struct timing *************************************************************/
  struct timing *t = &(chunk->times);
  pup_timing(p, t);
/*****************************************************************************/
//      printf("%d: Puped timing.\n", PRM->me);
}
#endif
