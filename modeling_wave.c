#include "modeling_wave.h"
#ifdef MODELING_WAVE
#include <unistd.h>
#include "modeling_papi.h"

FILE *fp_trace_wave = NULL;

double wave_ret;

static double x2x1, x4x3;
static int i, j, k;

static int filter_rank = 0;
static int iteration_start = 0;
static int iteration_end = 0;
static const char *wave_filename;

static int model_wave_trace_condition()
{
  if(model_rank == filter_rank &&
     model_timestep >= iteration_start && model_timestep <= iteration_end){
    return 1;
  }else{
    return 0;
  }
}

void model_wave_configuration(config_t * cfg)
{
  model_error_check(config_lookup_int(cfg, "wave.filter.rank", &filter_rank));
  model_error_check(config_lookup_int
		    (cfg, "wave.filter.iteration.start", &iteration_start));
  model_error_check(config_lookup_int
		    (cfg, "wave.filter.iteration.end", &iteration_end));

  model_error_check(config_lookup_string(cfg, "wave.trace", &wave_filename));
  //always append
  if(fp_trace_wave){
    fclose(fp_trace_wave);
  }
  if(access(wave_filename, F_OK) != -1){
    //file exists
    fp_trace_wave = fopen(wave_filename, "a");
    if(!fp_trace_wave){
      fprintf(stderr, "File %s could not be open for append.\n",
	      wave_filename);
      exit(123);
    }
  }else{
    fp_trace_wave = fopen(wave_filename, "w");
    if(!fp_trace_wave){
      fprintf(stderr, "File %s could not be open for write.\n",
	      wave_filename);
      exit(123);
    }
    model_wave_header();
  }
}

void model_wave_cpml4_start(double x2x1dif, double x4x3dif)
{
  x2x1 = x2x1dif;
  x4x3 = x4x3dif;
  if(model_wave_trace_condition()){
    model_papi_start_counters();
  }
}

void model_wave_cpml4_finish(void)
{
  if(model_wave_trace_condition()){
    model_papi_stop_counters();
    model_wave_report();
  }
}

void model_wave_header(void)
{
  fprintf(fp_trace_wave, "timestep rank operation imode i j k x2x1 x4x3 ");
  model_papi_header_with_fp(fp_trace_wave);
  fprintf(fp_trace_wave, "\n");
  fflush(fp_trace_wave);
}

void model_wave_report(void)
{
  char optypestr[TYPE_STR_LEN];
  model_mapping_name(model_operation, optypestr);
  fprintf(fp_trace_wave, "%d %d %s %d ", model_timestep, model_rank,
	  optypestr, model_imode);
  fprintf(fp_trace_wave, "%d %d %d %g %g ", i, j, k, x2x1, x4x3);
  model_papi_report_with_fp(fp_trace_wave);
  fprintf(fp_trace_wave, "\n");
  fflush(fp_trace_wave);
}

void model_wave_start(int ix, int jx, int kx)
{
  i = ix;
  j = jx;
  k = kx;
}

#endif
