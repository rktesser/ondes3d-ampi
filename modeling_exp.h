#ifndef __MODELING_EXP_H__
#define __MODELING_EXP_H__
#ifdef MODELING_TRACE_EXP
#include <math.h>

void model_exp_start(void);
void model_exp_configuration(config_t * cfg);
double myexp(double x);

#else

#define model_exp_start(A);
#define model_exp_configuration(A);

#endif //MODELING_TRACE_EXP
#endif //__MODELING_EXP_H__
