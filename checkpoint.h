#ifndef __CHECKPOINT_H_
#define __CHECKPOINT_H_
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/fcntl.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>

int checkpoint(const char *socket_str, const char *imgs_dir_str);
#endif // __CHECKPOINT_H_
