#ifndef __MODELING_H_
#define __MODELING_H_
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include "modeling_ifcounters.h"
#include "modeling_cpml4.h"
#include "modeling_intermediates.h"
#include "modeling_checkpoint.h"
#include "modeling_callgrind.h"
#include "modeling_exp.h"

#ifdef MODELING

#define TYPE_STR_LEN 15 // Maximum length of the operation names
#define TF_NAME_LEN 100 // Maximum length of the trace file name

typedef enum
{ OPSeisMoment, OPIntermediates, OPStress, OPVelocity } OPTYPE;

extern int model_rank;
extern int model_timestep;
extern int model_imode;
extern OPTYPE model_operation;

void model_init(int nprocs);
void model_finish(void);
void model_compute_start(OPTYPE type, int imode, int timestep);
void model_compute_finish(OPTYPE type, int imode, int timestep);
void model_read_configuration(int iteration);
void model_error_check(int returnCode);
void model_mapping_name(OPTYPE type, char *optypestr);

#else //MODELING

#define model_init(A);
#define model_finish();
#define model_compute_start(A,B,C);
#define model_compute_finish(A,B,C);
#define model_read_configuration(A);
#define model_error_check(A);
#define model_mapping_name(A);

#endif //MODELING
#endif //__MODELING_H_
