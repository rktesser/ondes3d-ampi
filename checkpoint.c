#include "checkpoint.h"
#include "rpc.pb-c.h"

#define MAX_MSG_SIZE 1024

static CriuResp *recv_resp(int socket_fd)
{
  unsigned char buf[MAX_MSG_SIZE];
  int len;
  CriuResp *msg = 0;

  len = read(socket_fd, buf, MAX_MSG_SIZE);
  if(len == -1){
    perror("Can't read response");
    return NULL;
  }

  msg = criu_resp__unpack(NULL, len, buf);
  if(!msg){
    perror("Failed unpacking response");
    return NULL;
  }

  return msg;
}

static int send_req(int socket_fd, CriuReq * req)
{
  unsigned char buf[MAX_MSG_SIZE];
  int len;

  len = criu_req__get_packed_size(req);

  if(criu_req__pack(req, buf) != len){
    perror("Failed packing request");
    return -1;
  }

  if(write(socket_fd, buf, len) == -1){
    perror("Can't send request");
    return -1;
  }

  return 0;
}

int checkpoint(const char *socket_str, const char *imgs_dir_str)
{
  CriuReq req = CRIU_REQ__INIT;
  CriuResp *resp = NULL;
  int fd, dir_fd;
  struct sockaddr_un addr;
  socklen_t addr_len;

  printf("Starting checkpointing...\n");

  if(!socket_str){
    fprintf(stderr, "socket string is NULL\n");
    return -1;
  }
  if(!imgs_dir_str){
    fprintf(stderr, "image directory strign is NULL\n");
    return -1;
  }

  /*
   * Open a directory, in which criu will
   * put images
   */
  printf("Opening directory to hold images...\n");
  dir_fd = open(imgs_dir_str, O_DIRECTORY);
  if(dir_fd == -1){
    printf("imgs_dir_str = %s\n", imgs_dir_str);
    perror("Can't open imgs dir");
    return -1;
  }
  /*
   * Set "DUMP" type of request.
   * Allocate CriuDumpReq.
   */
  printf("Prepare request information...\n");
  req.type = CRIU_REQ_TYPE__DUMP;
  req.opts = malloc(sizeof(CriuOpts));
  req.keep_open = false;
  req.notify_success = true;
  if(!req.opts){
    perror("Can't allocate memory for dump request");
    return -1;
  }
  criu_opts__init(req.opts);

  /*
   * Set dump options.
   * Checkout more in protobuf/rpc.proto.
   */
  req.opts->shell_job = true;
  req.opts->has_shell_job = true;
  req.opts->images_dir_fd = dir_fd;

  /*
   * Connect to service socket
   */
  printf("Connect to service socket...\n");
  fd = socket(AF_LOCAL, SOCK_SEQPACKET, 0);
  if(fd == -1){
    perror("Can't create socket");
    return -1;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_LOCAL;

  strcpy(addr.sun_path, socket_str);

  addr_len = strlen(addr.sun_path) + sizeof(addr.sun_family);

  int ret = connect(fd, (struct sockaddr *) &addr, addr_len);
  if(ret == -1){
    perror("Cant connect to socket");
    return -1;
  }

  /*
   * Send request
   */
  printf("Sending request...\n");
  ret = send_req(fd, &req);
  if(ret == -1){
    perror("Can't send request");
    return -1;
  }

  /*
   * Recv response
   */
  printf("Receiving response...\n");
  resp = recv_resp(fd);
  if(!resp){
    perror("Can't recv response");
    return -1;
  }

  if(resp->type != CRIU_REQ_TYPE__DUMP){
    perror("Unexpected response type");
    return -1;
  }

  /*
   * Check response.
   */
  printf("Response: %s\n", resp->cr_errmsg);
  printf("Check response... => %d\n", resp->success);
  if(resp->success == true){
    close(fd);
    close(dir_fd);
    return 0;
  }else{
    return -1;
  }
}
