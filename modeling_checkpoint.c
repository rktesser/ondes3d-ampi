#include "modeling_checkpoint.h"
#ifdef MODEL_CHECKPOINT

//Criu support
char *CRIU_SOCKET_FILE = NULL;
char *CRIU_ONDES3D_IMAGE_DIR = NULL;
int CRIU_CHECKPOINT_BY_RANK = -1;
int CRIU_CHECKPOINT_AT_ITERATION = -1;
int CRIU_CHECKPOINT_ACTIVE = 0;

void model_checkpoint(int iteration)
{
  if(model_rank != CRIU_CHECKPOINT_BY_RANK){
    return;
  }
  //check if we should checkpoint
  if(CRIU_CHECKPOINT_ACTIVE && iteration == CRIU_CHECKPOINT_AT_ITERATION){
    printf("Rank %d called the checkpoint procedure at iteration %d\n",
	   model_rank, iteration);
    if(checkpoint(CRIU_SOCKET_FILE, CRIU_ONDES3D_IMAGE_DIR) == 0){
      printf("Checkpoint worked! I am restarting now.\n");
    }else{
      printf("Checkpoint did not work.\n");
    }
  }
}

void model_checkpoint_configuration(config_t * cfg)
{
  //Criu checkpoint support
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "criu.checkpoint.by_rank",
		   &CRIU_CHECKPOINT_BY_RANK));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "criu.checkpoint.at_iteration",
		   &CRIU_CHECKPOINT_AT_ITERATION));
  CFG_ERROR_CHECK(config_lookup_bool
		  (&cfg, "criu.checkpoint.active", &CRIU_CHECKPOINT_ACTIVE));

  const char *aux;
  CFG_ERROR_CHECK(config_lookup_string(&cfg, "criu.socket", &aux));
  free(CRIU_SOCKET_FILE);
  CRIU_SOCKET_FILE = strdup(aux);

  CFG_ERROR_CHECK(config_lookup_string(&cfg, "criu.imagedir", &aux));
  free(CRIU_ONDES3D_IMAGE_DIR);
  CRIU_ONDES3D_IMAGE_DIR = strdup(aux);
}

#endif //MODEL_CHECKPOINT
