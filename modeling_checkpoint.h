#ifndef __MODEL_CHECKPOINT_H__
#define __MODEL_CHECKPOINT_H__
#ifdef MODELING_CHECKPOINT
#include <libconfig.h>

void model_checkpoint(int iteration);
void model_checkpoint_configuration(config_t * cfg);

#else

#define model_checkpoint(A);
#define model_checkpoint_configuration(A);

#endif //MODELING_CHECKPOINT
#endif //__MODEL_CHECKPOINT_H__
