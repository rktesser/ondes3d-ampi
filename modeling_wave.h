#ifndef __MODELING_WAVE_H__
#define __MODELING_WAVE_H__
#ifdef MODELING_WAVE
#include "modeling.h"
#include <libconfig.h>

void model_wave_cpml4_start(double x2x1dif, double x4x3dif);
void model_wave_cpml4_finish(void);
void model_wave_start(int i, int j, int k);
void model_wave_header(void);
void model_wave_report(void);
void model_wave_configuration(config_t * cfg);

extern double wave_ret;
#define CPML4(A,B,C,D,E,F,G,H,I,J,K) (model_wave_cpml4_start((I-H),(K-J)), wave_ret = CPML4(A,B,C,D,E,F,G,H,I,J,K), model_wave_cpml4_finish(), wave_ret)
//CPML4(A,B,C,D,E,F,G,H,I,J,K)


#else

#define model_wave_cpml4_start(A,B);
#define model_wave_cpml4_finish();
#define model_wave_start(A,B,C);
#define model_wave_configuration(A);
#endif // MODELING_WAVE
#endif //__MODELING_WAVE_H__
