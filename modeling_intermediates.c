#include "modeling_intermediates.h"
#ifdef MODELING_INTERMEDIATES

static int *Events;
static long_long *Values;
static int papi_nevents;

static int modeling_intermediates_filter_active;
static int modeling_intermediates_filter_rank;
static int modeling_intermediates_filter_timestep;
static const char *modeling_intermediates_filename;

static FILE *fp;
static struct timespec s, e;

static const char *mapping_name(OPInterType type)
{
  switch (type){
  case OPInterInit:
    return "Init";
  case OPInterElasticModel:
    return "ElasticModel";
  case OPInterElasticAbsorbing:
    return "ElasticAbsorbing";
  case OPInterElasticFreeABS:
    return "ElasticFreeABS";
  case OPInterAnelasticDAYandBRADLEY:
    return "AnelasticDB";
  case OPInterAnelasticKRISECandMOCZO:
    return "AnelasticKM";
  default:
    return "NA";
  }
}

static int model_inter_can_execute(void)
{
  if(!modeling_intermediates_filter_active)
    return 0;
  if(model_rank != modeling_intermediates_filter_rank ||
     model_timestep != modeling_intermediates_filter_timestep){
    return 0;
  }
  return 1;
}

void model_inter_configuration(config_t * cfg)
{
  CFG_ERROR_CHECK(config_lookup_bool
		  (&cfg, "intermediates.filter.active",
		   &modeling_intermediates_filter_active));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "intermediates.filter.rank",
		   &modeling_intermediates_filter_rank));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "intermediates.filter.timestep",
		   &modeling_intermediates_filter_timestep));
  free((char *) modeling_intermediates_filename);
  CFG_ERROR_CHECK(config_lookup_string
		  (&cfg, "intermediates.filename",
		   &modeling_intermediates_filename));
  modeling_intermediates_filename = strdup(modeling_intermediates_filename);
}

void model_inter_header(void)
{
  int i;
  //report hw counters
  fprintf(fp, " ");
  for(i = 0; i < papi_nevents; i++){
    char EventCodeStr[PAPI_MAX_STR_LEN];
    PAPI_event_code_to_name(Events[i], EventCodeStr);
    fprintf(fp, "%s", EventCodeStr);
    if(i + 1 != papi_nevents){
      fprintf(fp, " ");
    }
  }
}

void model_inter_init(void)
{
  if(!model_inter_can_execute())
    return;

  fp = fopen(modeling_intermediates_filename, "w");
  if(!fp){
    fprintf(stderr, "Could not open %s for writing.\n",
	    modeling_intermediates_filename);
  }
  fprintf(fp, "Rank Iteration Imode I J K Time Operation");

  model_inter_header();

  fprintf(fp, "\n");
}

void model_inter_finalize(void)
{
  if(!model_inter_can_execute())
    return;
  fclose(fp);
}

void model_inter_start(OPInterType type, int i, int j, int k)
{
  if(!model_inter_can_execute())
    return;

  PAPI_start_counters(Events, papi_nevents);
  clock_gettime(CLOCK_REALTIME, &s);
}

void model_inter_finish(OPInterType type, int i, int j, int k)
{
  if(!model_inter_can_execute())
    return;

  clock_gettime(CLOCK_REALTIME, &e);
  double dif = difftime(e.tv_sec, s.tv_sec) +
    ((double) (e.tv_nsec - s.tv_nsec)) / 1000000000.0;

  PAPI_stop_counters(Values, papi_nevents);

  //reporting data
  fprintf(fp, "%d %d %d %d %d %d %.9f %s", model_rank, model_timestep,
	  model_imode, i, j, k, dif, mapping_name(type));
  fprintf(fp, " ");
  //report hw counters
  int x;
  for(x = 0; x < papi_nevents; x++){
    fprintf(fp, "%lld", Values[x]);
    if(x + 1 != papi_nevents){
      fprintf(fp, " ");
    }
  }
  fprintf(fp, "\n");
}

#endif //MODELING_INTERMEDIATES
