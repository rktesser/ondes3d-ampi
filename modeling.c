#include <stdio.h>
#include <error.h>
#include <unistd.h>
#include <string.h>
#include "mpi.h"
#include "modeling.h"
#include "checkpoint.h"
#include "modeling_papi.h"
#include "modeling_wave.h"

//Configuration support
#include <libconfig.h>
#define MYSTRSIZE 1000
#define CFG_DEFAULT_PATH "modeling.cfg"

int model_rank;
int model_timestep;
int model_imode;
OPTYPE model_operation;

FILE *fp_model = NULL;
static double TIMER;

static inline double gettime(void)
{
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);	//should I use CLOCK_THREAD_CPUTIME_ID?
  return (double) tp.tv_sec + (double) tp.tv_nsec / 1E9;
}

void model_error_check(int returnCode)
{
  if(returnCode == CONFIG_FALSE){
    fprintf(stderr, "Failed reading configuration from %s.\n",
	    CFG_DEFAULT_PATH);
    fflush(stderr);
    exit(123);
  }
}

static void model_print_header()
{
  fprintf(fp_model, "Iteration Rank Operation Imode Time");

#if defined (MODELING_PAPI)
  fprintf(fp_model, " ");
#endif
  model_papi_header();

#if defined(COUNT_IFS)
  fprintf(fp_model, " ");
#endif
  model_ifcounters_header();

#if defined(MODELING_CPML4)
  fprintf(fp_model, " ");
#endif
  model_cpml4_header();

#if !defined(COUNT_IFS) || !defined(MODELING_PAPI) || !defined(MODELING_CPML4)
  fprintf(fp_model, "\n");
#endif

  fflush(fp_model);
}

void model_read_configuration(int iteration)
{
  FILE *fd = fopen(CFG_DEFAULT_PATH, "r");
  if(!fd){
    fprintf(stderr,
	    "it %d Configuration file %s coulnd't be open for reading.\n",
	    iteration, CFG_DEFAULT_PATH);
    return;
  }
  config_t cfg;
  config_init(&cfg);

  if(config_read(&cfg, fd) == CONFIG_FALSE){
    fprintf(stderr, "Failed to read the configuration file.\n");
    exit(123);
  }else{
    fclose(fd);
  }

  model_callgrind_configuration(&cfg);

  model_exp_configuration(&cfg);

  model_papi_affinity();

  model_papi_configuration(&cfg);

  model_wave_configuration(&cfg);

  //The global trace filename
  const char *trace_filename_prefix;
  model_error_check(config_lookup_string(&cfg, "filename",
	&trace_filename_prefix));
  
  char trace_filename[TF_NAME_LEN] = "\0";
  
  snprintf(trace_filename, TF_NAME_LEN, "%s-%d.csv", trace_filename_prefix,
      model_rank); 
  
  if(access(trace_filename, F_OK) != -1){
    //The trace file already exists, let's append to it
    if(fp_model)
      fclose(fp_model);
    fp_model = fopen(trace_filename, "a");
    if(!fp_model){
      fprintf(stderr, "The file %s could not be open for append.\n",
	      trace_filename);
      fflush(stderr);
      exit(123);
    }
  }else{
    //The trace file does not exists, create and dump header
    if(fp_model)
      fclose(fp_model);
    fp_model = fopen(trace_filename, "w");
    if(!fp_model){
      fprintf(stderr, "The file %s could not be open for write.\n",
	      trace_filename);
      fflush(stderr);
      exit(123);
    }
    model_print_header();
  }

  model_inter_configuration(&cfg);

  model_checkpoint_configuration(&cfg);

  //destroy configuration
  config_destroy(&cfg);
}


void model_init(int nprocs)
{
  model_papi_init();

  model_cpml4_init();
}

void model_finish(void)
{
  model_inter_finalize();
}

void model_mapping_name(OPTYPE type, char *optypestr)
{
  if(type == OPStress)
    strncpy(optypestr, "Stress", TYPE_STR_LEN);
  else if(type == OPVelocity)
     strncpy(optypestr, "Velocity", TYPE_STR_LEN);
  else if(type == OPIntermediates)
    strncpy(optypestr, "Intermediates", TYPE_STR_LEN);
  else if(type == OPSeisMoment)
    strncpy(optypestr, "SeisMoment", TYPE_STR_LEN);
  else
    strncpy(optypestr,"NA", TYPE_STR_LEN);
}

void model_compute_start(OPTYPE type, int imode, int timestep)
{
  //update imode
  model_imode = imode;
  //update operation
  model_operation = type;

  model_ifcounters_zero();

  model_cpml4_reset();

  model_papi_start_counters();

  model_callgrind_start();

  model_exp_start();

  TIMER = gettime();
}

void model_compute_finish(OPTYPE type, int imode, int timestep)
{
  model_callgrind_finish();

  char optypestr[TYPE_STR_LEN];
  model_mapping_name(type, optypestr);
  double dif = gettime() - TIMER;

  model_papi_stop_counters();

  TIMER = 0;

  fprintf(fp_model, "%d %d %s %d %.9lf", timestep, model_rank, optypestr, imode,
	dif);
#if defined(MODELING_PAPI)
  fprintf(fp_model, " ");
#endif
  model_papi_report();

  model_ifcounters_report();

#if defined(MODELING_CPML4)
  fprintf(fp_model, " ");
#endif
  model_cpml4_report();

#if !defined(COUNT_IFS) || !defined(MODELING_PAPI) || !defined(MODELING_CPML4)
  fprintf(fp_model, "\n");
#endif

  fflush(fp_model);
  //printf ("%d %s %s\n", model_rank, __FUNCTION__, mapping(type));
}
