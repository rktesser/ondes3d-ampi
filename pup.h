#ifndef MY_PUP_H_
#define MY_PUP_H_

#include "struct.h"
#include "nrutil.h"
#include "mpi.h"
#include "options.h"
#include "IO.h"
#include <stdlib.h>

#ifndef MPI_ONDES3D
void pup_parameters(pup_er p, void *prm_struct);
void pup_chunk(pup_er p, void *chunk);
#endif

#endif
