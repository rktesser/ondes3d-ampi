#ifndef __MODELING_IFCOUNTERS_H_
#define __MODELING_IFCOUNTERS_H_
#ifdef COUNT_IFS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
typedef enum
{ IF01_True, IF01_False,
  IF02_True, IF02_False,
  IF03_True, IF03_False,
  IF04_True, IF04_False,
  IF05_True, IF05_False,
  IF06_True, IF06_False,
  IF07_True, IF07_False,
  IF08_True, IF08_False,
  IF09_True, IF09_False,
  IFMax
} IFTYPE;
void model_ifcounters_header(void);
void model_ifcounters_zero(void);
void model_ifcounters_report(void);
void model_counter(int identifier);
#else

#define model_ifcounters_header(A)
#define model_ifcounters_zero(A)
#define model_ifcounters_report(A)
#define model_counter(A)
#endif //COUNT_IFS
#endif //__MODELING_IFCOUNTERS_H_
