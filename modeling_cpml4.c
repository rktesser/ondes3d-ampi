#include "modeling_cpml4.h"
#ifdef MODELING_CPML4
extern FILE *fp;		//defined in modeling.c

typedef enum
{
  cpml4_x2x1,
  cpml4_x4x3,

  staggardv4_x2x1,
  staggardv4_y2y1,
  staggardv4_z2z1,
  staggardv4_x4x3,
  staggardv4_y4y3,
  staggardv4_z4z3,

  staggards4_x2x1,
  staggards4_y2y1,
  staggards4_z2z1,
  staggards4_x4x3,
  staggards4_y4y3,
  staggards4_z4z3,

  staggardt4_x2x1,
  staggardt4_y2y1,
  staggardt4_x4x3,
  staggardt4_y4y3,

  diff4_x2x1,
  diff4_x4x3,

  MaxKernel
} KERNELTYPE;

long long int counter[MaxKernel];

static void ci(int val, int pos)
{
  if(val){
    counter[pos]++;
  }
}

static const char *model_code_to_name(int code)
{
  switch (code){

  case cpml4_x2x1:
    return "cpml4_x2x1";
  case cpml4_x4x3:
    return "cpml4_x4x3";

  case staggardv4_x2x1:
    return "staggardv4_x2x1";
  case staggardv4_y2y1:
    return "staggardv4_y2y1";
  case staggardv4_z2z1:
    return "staggardv4_z2z1";
  case staggardv4_x4x3:
    return "staggardv4_x4x3";
  case staggardv4_y4y3:
    return "staggardv4_y4y3";
  case staggardv4_z4z3:
    return "staggardv4_z4z3";

  case staggards4_x2x1:
    return "staggards4_x2x1";
  case staggards4_y2y1:
    return "staggards4_y2y1";
  case staggards4_z2z1:
    return "staggards4_z2z1";
  case staggards4_x4x3:
    return "staggards4_x4x3";
  case staggards4_y4y3:
    return "staggards4_y4y3";
  case staggards4_z4z3:
    return "staggards4_z4z3";

  case staggardt4_x2x1:
    return "staggardt4_x2x1";
  case staggardt4_y2y1:
    return "staggardt4_y2y1";
  case staggardt4_x4x3:
    return "staggardt4_x4x3";
  case staggardt4_y4y3:
    return "staggardt4_y4y3";

  case diff4_x2x1:
    return "diff4_x2x1";
  case diff4_x4x3:
    return "diff4_x4x3";

  default:
    return "none";
  }
}

void model_cpml4_init(void)
{
  model_cpml4_reset();
}

void model_cpml4_header(void)
{
  int i;
  for(i = 0; i < MaxKernel; i++){
    fprintf(fp, "%s", model_code_to_name(i));
    if(i + 1 != MaxKernel){
      fprintf(fp, " ");
    }
  }
}

void model_cpml4_report(void)
{
  int i;
  for(i = 0; i < MaxKernel; i++){
    fprintf(fp, "%lld", counter[i]);
    if(i + 1 != MaxKernel){
      fprintf(fp, " ");
    }
  }
}

void model_cpml4_reset(void)
{
  bzero(counter, MaxKernel * sizeof(long long int));
}

void model_cpml4_count(int x2x1, int x4x3)
{
  ci(x2x1, cpml4_x2x1);
  ci(x4x3, cpml4_x4x3);
}

void model_staggardv4_count(int x2x1, int y2y1, int z2z1, int x4x3, int y4y3,
			    int z4z3)
{
  ci(x2x1, staggardv4_x2x1);
  ci(y2y1, staggardv4_y2y1);
  ci(z2z1, staggardv4_z2z1);
  ci(x4x3, staggardv4_x4x3);
  ci(y4y3, staggardv4_y4y3);
  ci(z4z3, staggardv4_z4z3);
}

void model_staggards4_count(int x2x1, int y2y1, int z2z1, int x4x3, int y4y3,
			    int z4z3)
{
  ci(x2x1, staggards4_x2x1);
  ci(y2y1, staggards4_y2y1);
  ci(z2z1, staggards4_z2z1);
  ci(x4x3, staggards4_x4x3);
  ci(y4y3, staggards4_y4y3);
  ci(z4z3, staggards4_z4z3);
}

void model_staggardt4_count(int x2x1, int y2y1, int x4x3, int y4y3)
{
  ci(x2x1, staggardt4_x2x1);
  ci(y2y1, staggardt4_y2y1);
  ci(x4x3, staggardt4_x4x3);
  ci(y4y3, staggardt4_y4y3);
}

void model_diff4_count(int x2x1, int x4x3)
{
  ci(x2x1, diff4_x2x1);
  ci(x4x3, diff4_x4x3);
}

#endif //MODELING_CPML4
