#include "modeling_callgrind.h"
#ifdef MODELING_CALLGRIND

int MODELING_CALLGRIND_RANK = 0;
int MODELING_CALLGRIND_IMODE = 0;
int MODELING_CALLGRIND_ITERATION_START = 0;
int MODELING_CALLGRIND_ITERATION_END = 0;

void model_callgrind_configuration(config_t * cfg)
{
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "callgrind.filter.rank", &MODELING_CALLGRIND_RANK));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "callgrind.filter.imode",
		   &MODELING_CALLGRIND_FILTER_IMODE));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "callgrind.filter.iteration.start",
		   &MODELING_CALLGRIND_ITERATION_START));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "callgrind.filter.iteration.end",
		   &MODELING_CALLGRIND_ITERATION_END));
}

void model_callgrind_start(void)
{
  if(model_rank == MODELING_CALLGRIND_RANK &&
     timestep >= MODELING_CALLGRIND_ITERATION_START &&
     timestep <= MODELING_CALLGRIND_ITERATION_END &&
     imode == MODELING_CALLGRIND_IMODE){
    printf
      ("Activating callgrind instrumentation at rank = %d timestep = %d imode = %d\n",
       model_rank, timestep, imode);
    CALLGRIND_START_INSTRUMENTATION;
  }else{
    CALLGRIND_STOP_INSTRUMENTATION;
  }
}

void model_callgrind_finish(void)
{
  if(model_rank == MODELING_CALLGRIND_RANK &&
     timestep >= MODELING_CALLGRIND_ITERATION_START &&
     timestep <= MODELING_CALLGRIND_ITERATION_END &&
     imode == MODELING_CALLGRIND_IMODE){
    CALLGRIND_STOP_INSTRUMENTATION;
    printf
      ("Callgrind instrumentation has been stopped at rank = %d timestep = %d imode = %d\n",
       model_rank, timestep, imode);
  }else{
    CALLGRIND_STOP_INSTRUMENTATION;
  }
}
#endif //MODELING_CALLGRIND
