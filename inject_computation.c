#ifdef INJECT_COMPUTATION

#include <stdio.h>
#include <uthash.h>
#include "inject_computation.h"

#include "mpi.h"
#ifdef SIMGRID_VERSION
#include "pmpi_lb.h"
#endif


typedef struct{
  double computation;
  char key[OP_KEY_LENGHT];
  UT_hash_handle hh;
}op_comp_t;

op_comp_t *operations = NULL; // This is a hash table!

static op_comp_t * new_operation(int iteration, int rank, char *operation,
    int imode, double computation)
{
  op_comp_t * op = (op_comp_t *) malloc(sizeof(op_comp_t));
  if(op == NULL){
    fprintf(stderr, "ERROR! Could not allocate memory for operation.\n");
  }else{
    op->computation = computation;
    snprintf(op->key, OP_KEY_LENGHT, "%d-%d-%s-%d", iteration, rank, operation,
	  imode);
  }
  return op;
}

static int store_computation(int iteration, int rank, char *operation,
    int imode, double computation)
{
  op_comp_t * op = new_operation(iteration, rank, operation, imode,
      computation);
  if(op != NULL){
    HASH_ADD_STR(operations, key, op);
    return 1;
  }
  return 0;
}

int init_computation_model(char *filename_prefix)
{
  int rank, iteration, imode, ni;
  char operation[OP_NAME_LENGTH];
  double computation;
 
  char model_filename[TF_NAME_LEN] = "\0";

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  snprintf(model_filename, TF_NAME_LEN, "%s-%d.csv", filename_prefix, rank); 

  FILE *comp_model = fopen(model_filename, "r");
  if(!comp_model){
    fprintf(stderr, "ERROR! Could not open computation model file [%s].\n",
	model_filename);
    return 0;
  }
 
  
  char firstline[100];
  fgets(firstline, 100, comp_model); // ignore header
  
  do{ // read the file, line-by-line.
    ni = fscanf(comp_model, "%d %d %s %d %lf\n", &iteration, &rank,
	operation, &imode, &computation);
    if(ni == 5){
      if(!store_computation(iteration, rank, operation, imode, computation)){
	fprintf(stderr,
	    "ERROR! could not store operation in the hash table.\n");
        fclose(comp_model);
	return 0;
      }
    }else if(!feof(comp_model)){ 
	fprintf(stderr, "ERROR! Invalid line in computation model!\n");
	return 0;
    }
  }while(!feof(comp_model));

  fclose(comp_model);
  return 1;
}

double get_computation_for_operation(int iteration, int rank, char *operation,
    int imode)
{
  op_comp_t * op; 
  char key[OP_KEY_LENGHT];
  snprintf(key, OP_KEY_LENGHT, "%d-%d-%s-%d", iteration, rank, operation,
      imode);
  HASH_FIND_STR(operations, key, op);
  if(op == NULL){
    fprintf(stderr, "ERROR! Could not find operation with key '%s'\n", key);
    return -1;
  }
  return op->computation;
}

void destroy_computation_model(){
  op_comp_t *op, *tmp;
  HASH_ITER(hh, operations, op, tmp){
    HASH_DEL(operations, op);
    free(op);
  }
}

void inject_computation(double computation)
{
  smpilb_bench_end();  
  smpilb_execute(computation);
  smpilb_bench_begin();
}

#endif
