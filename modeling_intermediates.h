#ifndef __MODELING_INTERMEDIATES_H
#define __MODELING_INTERMEDIATES_H
#ifdef MODELING_INTERMEDIATES
#include <libconfig.h>
#include <time.h>
#include <papi.h>

typedef enum
{
  OPInterInit,
  OPInterElasticModel,
  OPInterElasticAbsorbing,
  OPInterElasticFreeABS,
  OPInterAnelasticDAYandBRADLEY,
  OPInterAnelasticKRISECandMOCZO
} OPInterType;

void model_inter_init(void);
void model_inter_configuration(config_t * cfg);
void model_inter_header(void);
void model_inter_start(OPInterType type, int i, int j, int k);
void model_inter_finish(OPInterType type, int i, int j, int k);
void model_inter_finalize(void);

//INIT
//ELASTIC (Absorbing + FreeABS) Model
//ELASTIC Absorbing ABCmethod
//ELASTIC FreeABS ABCmethod
//ANELASTIC Model ANLmethod DAYandBRADLEY
//ANELASTIC Model ANLmethod KRISTEKandMOCZO
//END of LOOP

#else

#define model_inter_init(A);
#define model_inter_configuration(A);
#define model_inter_header();
#define model_inter_start(A,B,C,D);
#define model_inter_finish(A,B,C,D);
#define model_inter_finalize(A);

#endif //MODELING_INTERMEDIATES
#endif // __MODELING_INTERMEDIATES_H
