#ifndef __MODELING_CALLGRIND_H__
#define __MODELING_CALLGRIND_H__
#ifdef MODELING_CALLGRIND
#include <valgrind/callgrind.h>

void model_callgrind_configuration(config_t * cfg);
void model_callgrind_start(void);
void model_callgrind_finish(void);

#else

#define model_callgrind_configuration(A);
#define model_callgrind_start(A);
#define model_callgrind_finish(A);

#endif //
#endif
