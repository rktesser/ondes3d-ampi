# Ondes3D with Adaptive MPI support

This is a modified version of the [Ondes3D](https://bitbucket.org/fdupros/ondes3d) seismic wave propagation simulator. 

This version supports ***dynamic load balancing*** using [Adaptive MPI (AMPI)](http://charm.cs.uiuc.edu/research/ampi/). This suppport was developed as part of [my PhD thesis](https://lume.ufrgs.br/handle/10183/180129) at the Federal University of Rio Grande do Sul (UFRGS), in a collaboration with the French Geolofical Survey (BRGM), as a member of the HPC-GA project. This PhD also included a collaboration with the Laboratoire d'Informatique de Grenoble (LIG), in France, where I worked during 11 months on a Sandwich PhD Internship.

Besides that, this code includes instrumentation and execution modeling code used in my PhD work. This was used, in the first place, to analyse the load balancing behavior of the Ondes3D (with AMPI and with the [SAMPI simulator](https://bitbucket.org/rktesser/smpi-lb)). And, in the second place, to implement two modeling techniques that help to speed-up the sequential emulation of the application with the [SimGrid simulator](http://simgrid.gforge.inria.fr).