#ifndef _NR_UTILS_H_
#define _NR_UTILS_H_

#include "mpi.h"		// Needed for the macro below.
#ifdef SIMGRID_VERSION
#include "pmpi_lb.h"		//Needed for overriding malloc.
#endif

#define NR_END 0
#define FREE_ARG char*

/* macro to convert defined argument to string :
   REF: http://en.wikipedia.org/wiki/C_preprocessor
 */
#define _QUOTEME(x) #x
#define QUOTEME(x) _QUOTEME(x)

//FIXME Lots of global variables
/*
static float sqrarg;
#define SQR(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)
*/
#define SQR(a) (a==0.0?0.0:(float)a*(float)a)

/*
static double dsqrarg;
#define DSQR(a) ((dsqrarg=(a)) == 0.0 ? 0.0 : dsqrarg*dsqrarg)
*/
#define DSQR(a) (a==0.0?0.0:(double)a*(double)a)

/*
static double dmaxarg1,dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ?\
        (dmaxarg1) : (dmaxarg2))
*/
#define MAX_VAL(a,b)((a) > (b) ? (a) : (b))
#define DMAX(a,b) MAX_VAL((double)a,(double)b)

/*
static double dminarg1,dminarg2;
#define DMIN(a,b) (dminarg1=(a),dminarg2=(b),(dminarg1) < (dminarg2) ?\
        (dminarg1) : (dminarg2))
*/
#define MIN_VAL(a,b) ((a) < (b) ? (a) : (b))
#define DMIN(a,b) MIN_VAL((double)a,(double)b)

/*
static float maxarg1,maxarg2;
#define FMAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ?\
        (maxarg1) : (maxarg2))
*/
#define FMAX(a,b) MAX_VAL((float)a,(float)b)

/*
static float minarg1,minarg2;
#define FMIN(a,b) (minarg1=(a),minarg2=(b),(minarg1) < (minarg2) ?\
        (minarg1) : (minarg2))
*/
#define FMIN(a,b) MIN_VAL((float)a,(float)b)

/*
static long lmaxarg1,lmaxarg2;
#define LMAX(a,b) (lmaxarg1=(a),lmaxarg2=(b),(lmaxarg1) > (lmaxarg2) ?\
        (lmaxarg1) : (lmaxarg2))
*/
#define LMAX(a,b) MAX_VAL((long)a,(long)b)

/*
static long lminarg1,lminarg2;
#define LMIN(a,b) (lminarg1=(a),lminarg2=(b),(lminarg1) < (lminarg2) ?\
        (lminarg1) : (lminarg2))
*/
#define LMIN(a,b) MIN_VAL((long)a,(long)b)

/*
static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ?\
        (imaxarg1) : (imaxarg2))
*/
#define IMAX(a,b) MAX_VAL((int)a,(int)b)

/*
static int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ?\
        (iminarg1) : (iminarg2))
*/

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

/******************************************************************************/

void nrerror(char error_text[]);
float *vector(long nl, long nh);
int *ivector(long nl, long nh);
unsigned char *cvector(long nl, long nh);
unsigned long *lvector(long nl, long nh);
double *dvector(const long nl, const long nh);
double *dvector0(long nl, long nh);
float **matrix(long nrl, long nrh, long ncl, long nch);
double **dmatrix(long nrl, long nrh, long ncl, long nch);
double **dmatrix0(long nrl, long nrh, long ncl, long nch);
int **imatrix(long nrl, long nrh, long ncl, long nch);
float **submatrix(float **a, long oldrl, long oldrh, long oldcl, long oldch,
		  long newrl, long newcl);
float **convert_matrix(float *a, long nrl, long nrh, long ncl, long nch);
int ***i3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
float ***f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
double ***d3tensor(long nrl, long nrh, long ncl, long nch, long ndl,
		   long ndh);
void free_vector(float *v, long nl, long nh);
void free_ivector(int *v, long nl, long nh);
void free_cvector(unsigned char *v, long nl, long nh);
void free_lvector(unsigned long *v, long nl, long nh);
void free_dvector(double *v, long nl, long nh);
void free_dvector0(double *v, long nl, long nh);
void free_matrix(float **m, long nrl, long nrh, long ncl, long nch);
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch);
void free_dmatrix0(double **m, long nrl, long nrh, long ncl, long nch);
void free_imatrix(int **m, long nrl, long nrh, long ncl, long nch);
void free_submatrix(float **b, long nrl, long nrh, long ncl, long nch);
void free_convert_matrix(float **b, long nrl, long nrh, long ncl, long nch);
void free_i3tensor(int ***t, long nrl, long nrh, long ncl, long nch,
		   long ndl, long ndh);
void free_f3tensor(float ***t, long nrl, long nrh, long ncl, long nch,
		   long ndl, long ndh);
void free_d3tensor(double ***t, long nrl, long nrh, long ncl, long nch,
		   long ndl, long ndh);
void free_d3tensor0(double ***t, long nrl, long nrh, long ncl, long nch,
		    long ndl, long ndh);


double ***myd3tensor0(long nrl, long nrh, long ncl, long nch, long ndl,
		      long ndh);
double **mydmatrix0(long nrl, long nrh, long ncl, long nch);
double *mydvector0(const long nl, const long nh);
double *mydvectorRealloc(double *v, long nl, long nh);

#endif /* _NR_UTILS_H_ */
//1st pass
//2nd pass
