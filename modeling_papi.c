#include "modeling_papi.h"
#ifdef MODELING_PAPI
extern FILE *fp_model;		//defined in modeling.c

int *Events;
long_long *Values;
int papi_nevents;

void model_papi_init()
{
  PAPI_library_init(PAPI_VER_CURRENT);
  Events = malloc(PAPI_MAX_HWCTRS * sizeof(int));
  Values = malloc(PAPI_MAX_HWCTRS * sizeof(long_long));
  papi_nevents = 0;
}

void model_papi_start_counters()
{
  PAPI_start_counters(Events, papi_nevents);
}

void model_papi_stop_counters()
{
  PAPI_stop_counters(Values, papi_nevents);
}

void model_papi_header()
{
  model_papi_header_with_fp(fp_model);
}

void model_papi_header_with_fp(FILE * pfp)
{
  int i;
  //report hw counters
  for(i = 0; i < papi_nevents; i++){
    char EventCodeStr[PAPI_MAX_STR_LEN];
    PAPI_event_code_to_name(Events[i], EventCodeStr);
    fprintf(pfp, "%s", EventCodeStr);
    if(i + 1 != papi_nevents){
      fprintf(pfp, " ");
    }
  }
}

void model_papi_report()
{
  model_papi_report_with_fp(fp_model);
}

void model_papi_report_with_fp(FILE * pfp)
{
  int i;
  //report hw counters
  for(i = 0; i < papi_nevents; i++){
    fprintf(pfp, "%lld", Values[i]);
    if(i + 1 != papi_nevents){
      fprintf(pfp, " ");
    }
  }
}

void model_papi_affinity()
{
  //core affinity
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(0, &mask);
  if(sched_setaffinity(0, sizeof(mask), &mask) < 0){
    fprintf(stderr, "Failed to set core affinity.\n");
    exit(1);
  }
}

void model_papi_configuration(config_t * cfg)
{
  config_setting_t *sub_cfg;
  papi_nevents = 0;
  sub_cfg = config_lookup(cfg, "papi.counters");
  const char *papi_event_name;
  int i;
  for(i = 0; (papi_event_name = config_setting_get_string_elem(sub_cfg, i));
      i++){
    if(PAPI_event_name_to_code((char *) papi_event_name, &Events[i]) !=
       PAPI_OK){
      fprintf(stderr, "Failed to codify papi event [%s].\n", papi_event_name);
      exit(1);
    }
    papi_nevents++;
  }
  papi_nevents = i;
}
#endif //MODELING_PAPI
