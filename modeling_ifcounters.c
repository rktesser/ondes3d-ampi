#include "modeling_ifcounters.h"
#ifdef COUNT_IFS

extern FILE *fp;		//defined in modeling.c
static unsigned long long ifcounters[IFMax];

void model_ifcounters_header(void)
{
  int i;
  //report if counters
  fprintf(fp, " ");
  for(i = 0; i < IFMax / 2; i++){
    int aux = i + 1;
    fprintf(fp, "IF%02dTrue IF%02dFalse", aux, aux);
    if(i + 1 == IFMax / 2){
      fprintf(fp, "\n");
    }else{
      fprintf(fp, " ");
    }
  }
}

void model_ifcounters_zero(void)
{
  int i;
  //zeroing ifcounters for this rank
  for(i = 0; i < IFMax; i++){
    ifcounters[i] = 0;
  }
}

void model_ifcounters_report(void)
{
  int i;
  //report if counters
  fprintf(fp, " ");
  for(i = 0; i < IFMax; i++){
    fprintf(fp, "%llu", ifcounters[i]);
    if(i + 1 == IFMax){
      fprintf(fp, "\n");
    }else{
      fprintf(fp, " ");
    }
  }
}

void model_counter(int identifier)
{
  ifcounters[identifier]++;
}

#endif
