#include "modeling_exp.h"
#ifdef MODELING_TRACE_EXP

FILE *fp_trace_exp = NULL;
const char **TRACE_EXP_FILENAME;
//to filter
int TRACE_EXP_FILTER_RANK = 0;
int TRACE_EXP_FILTER_ITERATION_START = 0;
int TRACE_EXP_FILTER_ITERATION_END = 0;
//to correctly trace in myexp call
int TRACE_EXP_CUR_TYPE = 0;
int TRACE_EXP_CUR_RANK = 0;
int TRACE_EXP_CUR_IMODE = 0;
int TRACE_EXP_CUR_ITERATION = 0;

void model_exp_start(void)
{
  TRACE_EXP_CUR_TYPE = type;
  TRACE_EXP_CUR_RANK = model_rank;
  TRACE_EXP_CUR_IMODE = imode;
  TRACE_EXP_CUR_ITERATION = timestep;
}

void model_exp_configuration(config_t * cfg)
{
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfd, "exp.filter.rank", &TRACE_EXP_FILTER_RANK));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "exp.filter.iteration.start",
		   &TRACE_EXP_FILTER_ITERATION_START));
  CFG_ERROR_CHECK(config_lookup_int
		  (&cfg, "exp.filter.iteration.end",
		   &TRACE_EXP_FILTER_ITERATION_END));


  CFG_ERROR_CHECK(config_lookup_string
		  (&cfg, "exp.trace", &TRACE_EXP_FILENAME));
  //always append
  fp_trace_exp = fopen(TRACE_EXP_FILENAME, "a");

}

double myexp(double x)
{
  if(TRACE_EXP_CUR_RANK == TRACE_EXP_FILTER_RANK &&
     TRACE_EXP_CUR_ITERATION >= TRACE_EXP_FILTER_ITERATION_START &&
     TRACE_EXP_CUR_ITERATION <= TRACE_EXP_FILTER_ITERATION_END){
    fprintf(fp_trace_exp, "%g %s %d %d %d\n", x,
	    mapping_name(TRACE_EXP_CUR_TYPE), TRACE_EXP_FILTER_RANK,
	    TRACE_EXP_CUR_IMODE, TRACE_EXP_CUR_ITERATION);
  }
  return exp(x);
}
#endif //MODELING_TRACE_EXP
