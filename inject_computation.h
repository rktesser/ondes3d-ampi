#ifndef _INJECT_COMPUTATION_H
#define _INJECT_COMPUTATION_H

#define OP_KEY_LENGHT 80
#define OP_NAME_LENGTH 15

#ifndef TF_NAME_LEN
#define TF_NAME_LEN 100
#endif

#ifdef INJECT_COMPUTATION

int init_computation_model(char *filename_prefix);
double get_computation_for_operation(int rank, int iteration, char *operation,
    int imode);
void destroy_computation_model();
void inject_computation(double computation);
#endif

#endif
