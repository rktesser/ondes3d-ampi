#ifndef MODELING_CPML4_H
#define MODELING_CPML4_H_
#ifdef MODELING_CPML4
#include <stdio.h>
#include <strings.h>

void model_cpml4_init(void);
void model_cpml4_header(void);
void model_cpml4_report(void);
void model_cpml4_reset(void);
void model_cpml4_count(int x2x1, int x4x3);
void model_staggardv4_count(int x2x1, int y2y1, int z2z1,
			    int x4x3, int y4y3, int z4z3);
void model_staggards4_count(int x2x1, int y2y1, int z2z1,
			    int x4x3, int y4y3, int z4z3);
void model_staggardt4_count(int x2x1, int y2y1, int x4x3, int y4y3);
void model_diff4_count(int x2x1, int x4x3);

#else

#define model_cpml4_init(A)
#define model_cpml4_header(A)
#define model_cpml4_report(A)
#define model_cpml4_reset(A)
#define model_cpml4_count(A,B)
#define model_staggardv4_count(A,B,C,D,E,F)
#define model_staggards4_count(A,B,C,D,E,F)
#define model_staggardt4_count(A,B,C,D)
#define model_diff4_count(A,B)

#endif //MODELING_CPML4
#endif //MODELING_CPML4_H_
